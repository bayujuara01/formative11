/*
 Author : Bayu Seno Ariefyanto
 Formative : Day 11 Task 6
*/

/* Problem

Implementasikan function graduates untuk mendapatkan 
daftar student yang lulus dengan aturan:

Student dapat dinyatakan lulus apabila score lebih besar dari 75.
Masukkan name dan score dari student ke class yang dia ikuti.
Student yang tidak lulus tidak perlu ditampilkan.
Output yang diharapkan berupa Object Literal dengan format sebagai berikut:

{
  <class>: [
    { name: <name>, score: <score> },
    ...
  ],
  <class>: [
    { name: <name>, score: <score> },
    ...
  ],
  <class>: [] //NOTE: Jika tidak ada student yang lulus, class ini akan diisi oleh array kosong
}

*/

function graduates (students) {
  const graduatesStudent = students.filter(student => student.score >= 75);
  const graduates = {};
//   console.log(graduatesStudent);

  for (const graduate of graduatesStudent) {

      if (graduates[graduate.class]) {
        graduates[graduate.class].push({
          name : graduate.name,
          score: graduate.score
        });
      } else {
        graduates[graduate.class] = [];
        graduates[graduate.class].push({
          name : graduate.name,
          score: graduate.score
        });
      }
  }

  return graduates;
    
}

console.log(graduates([
  {
    name: 'Dimitri',
    score: 90,
    class: 'A'
  },
  {
    name: 'Alexei',
    score: 85,
    class: 'B'
  },
  {
    name: 'Sergei',
    score: 74,
    class: 'A'
  },
  {
    name: 'Anastasia',
    score: 78,
    class: 'B'
  }
]));

// {
//   A: [
//     { name: 'Dimitri', score: 90 }
//   ],
//   B: [
//     { name: 'Alexei' , score: 85 },
//     { name: 'Anastasia', score: 78 }
//   ]
// }

console.log(graduates([
  {
    name: 'Alexander',
    score: 100,
    class: 'A'
  },
  {
    name: 'Alisa',
    score: 76,
    class: 'B'
  },
  {
    name: 'Vladimir',
    score: 92,
    class: 'A'
  },
  {
    name: 'Albert',
    score: 71,
    class: 'B'
  },
  {
    name: 'Viktor',
    score: 80,
    class: 'C'
  }
]));

// {
//   A: [
//     { name: 'Alexander', score: 100 },
//     { name: 'Vladimir', score: 92 }
//   ],
//   B: [
//     { name: 'Alisa', score: 76 },
//   ],
//   C: [
//     { name: 'Viktor', score: 80 }
//   ]
// }


console.log(graduates([])); //{}