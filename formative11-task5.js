/*
 Author : Bayu Seno Ariefyanto
 Formative : Day 11 Task 5
*/

/* Problem

implementasikan function highestRanking untuk 
mendapatkan student dengan nilai tertinggi 
dari setiap class:

Output yang diharapkan berupa Object Literal dengan format sebagai berikut:

{
  <class>: { name: <name>, score: <score> },
  <class>: { name: <name>, score: <score> },
  <class>: { name: <name>, score: <score> }
}

*/

function highestRanking (students) {
  let highest = {};
  let highestScore = 0;

  for (const student of students) {
      if (student.score > highestScore) {
          highest = student;
          highestScore = student.score;
      }
  }

  return highest;
}

// TEST CASE
console.log(highestRanking([
  {
    name: 'Dimitri',
    score: 90,
    class: 'A'
  },
  {
    name: 'Alexei',
    score: 85,
    class: 'B'
  },
  {
    name: 'Sergei',
    score: 74,
    class: 'A'
  },
  {
    name: 'Anastasia',
    score: 78,
    class: 'B'
  }
]));

// {
//   A: { name: 'Dimitri', score: 90 },
//   B: { name: 'Alexei', score: 85 }
// }


console.log(highestRanking([
  {
    name: 'Alexander',
    score: 100,
    class: 'A'
  },
  {
    name: 'Alisa',
    score: 76,
    class: 'B'
  },
  {
    name: 'Vladimir',
    score: 92,
    class: 'A'
  },
  {
    name: 'Albert',
    score: 71,
    class: 'B'
  },
  {
    name: 'Viktor',
    score: 80,
    class: 'C'
  }
]));

// {
//   A: { name: 'Alexander', score: 100 },
//   B: { name: 'Alisa', score: 76 },
//   C: { name: 'Viktor', score: 80 }
// }


console.log(highestRanking([])); //{}