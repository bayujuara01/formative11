/*
 Author : Bayu Seno Ariefyanto
 Formative : Day 11 Task 3
*/
/* Problem

Diberikan function changeAlphabet(kata) yang akan menerima 
satu parameter berupa string. Function akan me-return 
sebuah kata baru dimana setiap huruf akan digantikan dengan 
huruf alfabet setelahnya. 
Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.

 */

function changeAlphabet(kata) {
  let nextKata = "";
  for (let i = 0; i < kata.length; i++) {
      nextKata += String.fromCharCode(kata[i].toLowerCase().charCodeAt() + 1);
  }
  return nextKata;
}

// TEST CASES
console.log(changeAlphabet('wow')); // xpx
console.log(changeAlphabet('developer')); // efwfmpqfs
console.log(changeAlphabet('javascript')); // kbwbtdsjqu
console.log(changeAlphabet('keren')); // lfsfo
console.log(changeAlphabet('semangat')); // tfnbohbu