/*
 Author : Bayu Seno Ariefyanto
 Formative : Day 11 Task 2
*/
/* Problem

Diberikan sebuah function groupAnimals(arr) yang menerima
satu parameter berupa array yang berisi string. 
Function akan me-return array multidimensi dimana array tersebut 
berisikan kategori/kelompok array yang dikumpulkan sesuai dengan 
abjad depannya. Abjad harus berurut dari a-z (jika ada). 
Untuk kasus ini, anggap saja semua text lowercase.

Contoh jika animals adalah 
["ayam", "kucing", "bebek", "bangau", "zebra"]
maka akan menampilkan output: 
[ [ "ayam" ], [ "bebek", "bangau" ], [ "kucing ], [ "zebra" ] ]

 */

function groupAnimals(animals) {
  // you can only write your code here!
  const animalsObject = {};
  
  for (let animal of animals) {
      if (animalsObject[animal[0]]) {
          animalsObject[animal[0]].push(animal);
      } else {
          animalsObject[animal[0]] = [];
          animalsObject[animal[0]].push(animal);
      }
  }  

  const keys = Object.keys(animalsObject);
  keys.sort();
  const results = [];

  for (let i = 0 ; i < keys.length; i++) {
      results.push(animalsObject[keys[i]]);
  }

  return results;
}

// TEST CASES
console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil']));
// [ ['ayam', 'anoa'], ['cacing'], ['kuda', 'kancil'] ]
console.log(groupAnimals(['cacing', 'ayam', 'kuda', 'anoa', 'kancil', 'unta', 'cicak' ]));
// [ ['ayam', 'anoa'], ['cacing', 'cicak'], ['kuda', 'kancil'], ['unta'] ]