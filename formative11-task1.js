/*
 Author : Bayu Seno Ariefyanto
 Formative : Day 11 Task 1
*/
/* Problem

Diberikan sebuah function groupNumber(arr) yang 
menerima satu parameter berupa array yang berisi angka-angka. 
Function akan me-return array multidimensi dimana array tersebut 
berisikan 3 kategori/kelompok:

kelompok pertama (baris pertama) merupakan angka-angka genap
kelompok ke-2 (baris ke-2) merupakan angka-angka ganjil
kelompok ke-3 (baris ke-3) merupakan angka-angka kelipatan 3
Contoh jika arr inputan adalah [45, 20, 21, 2, 7] 
maka output: [ [ 20, 2 ], [ 7 ], [ 45, 21 ] ]

 */

const groupNumber = (numbers) => {
    const odds = [];
    const evens = [];
    const triplet = [];
    for (const num of numbers) {
        if (num % 3 === 0) {
            triplet.push(num);
        } else if (num % 2 === 0) {
           evens.push(num);
        } else {
           odds.push(num);
        }
    }

    return [evens, odds, triplet];
}

console.log(groupNumber([2, 4, 6])); // [ [2, 4], [], [6] ]
console.log(groupNumber([1, 2, 3, 4, 5, 6, 7, 8, 9])); // [ [ 2, 4, 8 ], [ 1, 5, 7 ], [ 3, 6, 9 ] ]
console.log(groupNumber([100, 151, 122, 99, 111])); // [ [ 100, 122 ], [ 151 ], [ 99, 111 ] ]
console.log(groupNumber([])); // [ [], [], [] ]